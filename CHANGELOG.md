# Changelog

## [v1.2.0-SNAPSHOT]
 - Updated to gcube-bom 2.4.1



## [v1.1.0] - 2020-06-05

- Added the property cataloguePortletURL and the method [#19440]



## [v1.0.0] - 2020-04-01

- First release



This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).