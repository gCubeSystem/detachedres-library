/**
 * 
 */
package org.gcube.infrastructure.detachedres.detachedreslibrary;

import java.io.File;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.DetachedREsClient;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.obj.DetachedREsJAXB;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.obj.GatewayJAXB;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.obj.VOJAXB;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.obj.VREJAXB;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.Constants;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.DetachedREs;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.Gateway;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VO;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VRE;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import junit.framework.TestCase;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class DetachedREsTest extends TestCase {

	private static Logger logger = LoggerFactory.getLogger(DetachedREsTest.class);
	
	@Ignore
	@Test
	public void testDetachedREsResource() {
		if (Constants.TEST_ENABLE) {
			logger.debug("Test Enabled");

			try {
				ScopeProvider.instance.set(Constants.DEFAULT_SCOPE);
				SecurityTokenProvider.instance.set(Constants.DEFAULT_TOKEN);
				DetachedREsClient detachedREsClient = new DetachedREsClient();
				DetachedREs detachedREs = detachedREsClient.getDetachedREs();

				int totalVREDimissed = 0;
				for (Gateway gateway : detachedREs.getGateways().values()) {
					logger.debug("\n\n");
					logger.debug("Gateway: " + gateway.getName());
					int vreDismissedPerGatew = 0;
					for (VO vo : gateway.getVos().values()) {
						logger.debug("VO: " + vo.getName());
						for (VRE vre : vo.getVres().values()) {
							logger.debug("VRE name: " + vre.getName() + " scope: " + vre.getScope()
									+ " VRE catalogue url: " + vre.getCatalogUrl() + " VRE catalog Portlet URL: "
									+ vre.getCatalogPortletURL());
							vreDismissedPerGatew++;
						}
					}
					logger.debug("\n\t\t\t******** VREs dismissed per " + gateway.getName() + " are: "
							+ vreDismissedPerGatew);
					totalVREDimissed += vreDismissedPerGatew;
				}

				logger.debug("\n\nTotal VREs dismissed: " + totalVREDimissed);

				assertTrue(true);

			} catch (Exception e) {
				logger.error(e.getLocalizedMessage(), e);
				assertTrue("Error searching the resource!", false);
			}

		} else {
			logger.debug("Test Disabled");
			assertTrue(true);
		}
	}

	@Ignore
	@Test
	public void testFindDetachedVREForVREName() {
		if (Constants.TEST_ENABLE) {
			logger.debug("Test Enabled");

			try {
				String vreName = "bluebridgeproject";
				ScopeProvider.instance.set(Constants.DEFAULT_SCOPE);
				SecurityTokenProvider.instance.set(Constants.DEFAULT_TOKEN);
				DetachedREsClient detachedREsClient = new DetachedREsClient();
				VRE vre = detachedREsClient.findDetachedVREforVREName(vreName);
				logger.debug("The detached VRE for name: " + vreName + " found as: " + vre);
				assertTrue(true);

			} catch (Exception e) {
				logger.error(e.getLocalizedMessage(), e);
				assertTrue("Error searching the resource!", false);
			}

		} else {
			logger.debug("Test Disabled");
			assertTrue(true);
		}
	}

	@Ignore
	@Test
	public void testDetachedREsMarshaller() {
		if (Constants.TEST_ENABLE) {
			logger.debug("Test Enabled");

			try {
				logger.info("Check Marshalling");

				GatewayJAXB blueBridgeGateway = new GatewayJAXB();
				blueBridgeGateway.setScope("-1");
				blueBridgeGateway.setName("BlueBridge Gateway");

				VOJAXB gCubeApps = new VOJAXB();
				gCubeApps.setScope("/d4science.research-infrastructures.eu/gCubeApps");
				gCubeApps.setName("gCubeApps");

				VREJAXB blueBridgeProject = new VREJAXB();
				blueBridgeProject.setScope("/d4science.research-infrastructures.eu/gcubeApps/BlueBridgeProject");
				blueBridgeProject.setName("BlueBridgeProject");
				blueBridgeProject.setStartDate(new GregorianCalendar(2018, GregorianCalendar.JANUARY, 1).getTime());
				blueBridgeProject.setEndDate(new GregorianCalendar(2020, GregorianCalendar.MARCH, 3).getTime());
				blueBridgeProject.setCatalogUrl("http://data.d4science.org/ctlg/BlueBridgeProject");
				blueBridgeProject
						.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));

				LinkedHashMap<String, VREJAXB> vres = new LinkedHashMap<>();
				vres.put("/d4science.research-infrastructures.eu/gcubeApps/BlueBridgeProject", blueBridgeProject);
				gCubeApps.setVres(vres);

				LinkedHashMap<String, VOJAXB> vos = new LinkedHashMap<>();
				vos.put("/d4science.research-infrastructures.eu/gCubeApps", gCubeApps);
				blueBridgeGateway.setVos(vos);

				LinkedHashMap<String, GatewayJAXB> gateways = new LinkedHashMap<>();
				gateways.put("-1", blueBridgeGateway);

				DetachedREsJAXB detachedREs = new DetachedREsJAXB();
				detachedREs.setEnabled(true);
				detachedREs.setGateways(gateways);

				JAXBContext jaxbContext = JAXBContext.newInstance(DetachedREsJAXB.class);
				Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
				jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

				// Print XML String to Console
				StringWriter sw = new StringWriter();

				// Write XML to StringWriter
				jaxbMarshaller.marshal(detachedREs, sw);

				// Verify XML Content
				String xmlContent = sw.toString();
				logger.debug(xmlContent);

				logger.info("Check Unmarshalling");
				Path path = Files.createTempFile("DetachedREsData", ".xml");
				File file = path.toFile();
				jaxbMarshaller.marshal(detachedREs, file);

				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				DetachedREsJAXB configUnmarshalled = (DetachedREsJAXB) jaxbUnmarshaller.unmarshal(file);
				logger.debug("DetachedREs unmarshallded: " + configUnmarshalled);
				file.delete();
				logger.info("Success!");
				assertTrue(true);

			} catch (Throwable e) {
				logger.error(e.getLocalizedMessage(), e);
				assertTrue("Error in DetachedREs Marshal!", false);
			}
		} else {
			logger.debug("Test Disabled");
			assertTrue(true);
		}
	}

	@Ignore
	@Test
	public void testDetachedREsUnMarshaller() {
		if (Constants.TEST_ENABLE) {
			logger.debug("Test Enabled");

			try {
				logger.info("Check UnMarshalling");
				JAXBContext jaxbContext = JAXBContext.newInstance(DetachedREsJAXB.class);
				
				Path path = Paths.get("DetachedREsData.xml");
				logger.info("File: " + path.toAbsolutePath());
				File file = path.toFile();
				
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				DetachedREsJAXB configUnmarshalled = (DetachedREsJAXB) jaxbUnmarshaller.unmarshal(file);
				logger.debug("DetachedREs unmarshallded: " + configUnmarshalled);
				logger.info("Success!");
				assertTrue(true);

			} catch (Throwable e) {
				logger.error(e.getLocalizedMessage(), e);
				assertTrue("Error in DetachedREs UnMarshal!", false);
			}
		} else {
			logger.debug("Test Disabled");
			assertTrue(true);
		}
	}		
	
	@Ignore	
	@Test
	public void testDetachedREsMarshallerProd() {
		if (Constants.TEST_ENABLE) {
			logger.debug("Test Enabled");

			try {
				logger.info("Check Marshalling");

				// final String GCUBEAPPS = "gCubeApps";
				// final String FARM = "FARM";
				// final String D4RESEARCH = "D4Research";
				// final String PARTHENOSVO = "ParthenosVO";
				// final String D4OS = "D4OS";

				// ------ BlueBridge Gateway begin
				GatewayJAXB blueBridgeGateway = new GatewayJAXB();
				blueBridgeGateway.setScope("-1");
				blueBridgeGateway.setName("BlueBridge Gateway");

				createBlueBridgeGateway(blueBridgeGateway);

				// ------ D4Science.org Detached Gateway begin
				GatewayJAXB d4ScienceOrgDetachedGateway = new GatewayJAXB();
				d4ScienceOrgDetachedGateway.setScope("-2");
				d4ScienceOrgDetachedGateway.setName("D4Science.org Detached Gateway");

				createD4ScienceOrgDetachedGateway(d4ScienceOrgDetachedGateway);

				// ------ PARTHENOS Detached Gateway begin
				GatewayJAXB parthenosDetachedGateway = new GatewayJAXB();
				parthenosDetachedGateway.setScope("-3");
				parthenosDetachedGateway.setName("PARTHENOS Detached Gateway");

				createParthenosDetachedGateway(parthenosDetachedGateway);

				// ------ AGINFRAPlus Detached Gateway begin
				GatewayJAXB aginfraPlusDetachedGateway = new GatewayJAXB();
				aginfraPlusDetachedGateway.setScope("-4");
				aginfraPlusDetachedGateway.setName("AGINFRAPlus Detached Gateway");

				createAginfraPlusDetachedGateway(aginfraPlusDetachedGateway);

				// ------ EOSC-Secretriat Detached Gateway begin
				GatewayJAXB eoscSecretariatDetachedGateway = new GatewayJAXB();
				eoscSecretariatDetachedGateway.setScope("-5");
				eoscSecretariatDetachedGateway.setName("EOSC-Secretariat Detached Gateway");

				createEOSCSecretariatDetachedGateway(eoscSecretariatDetachedGateway);

				// ------ DESCRAMBLE Gateway begin
				GatewayJAXB descrambleGateway = new GatewayJAXB();
				descrambleGateway.setScope("-6");
				descrambleGateway.setName("DESCRAMBLE Gateway");

				createDescrambleGateway(descrambleGateway);

				// Gateway Add
				// -------------------

				LinkedHashMap<String, GatewayJAXB> gateways = new LinkedHashMap<>();
				gateways.put("-1", blueBridgeGateway);
				gateways.put("-2", d4ScienceOrgDetachedGateway);
				gateways.put("-3", parthenosDetachedGateway);
				gateways.put("-4", aginfraPlusDetachedGateway);
				gateways.put("-5", eoscSecretariatDetachedGateway);
				gateways.put("-6", descrambleGateway);

				DetachedREsJAXB detachedREs = new DetachedREsJAXB();
				detachedREs.setEnabled(true);
				detachedREs.setGateways(gateways);

				JAXBContext jaxbContext = JAXBContext.newInstance(DetachedREsJAXB.class);
				Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
				jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

				// Print XML String to Console
				StringWriter sw = new StringWriter();

				// Write XML to StringWriter
				jaxbMarshaller.marshal(detachedREs, sw);

				// Verify XML Content
				String xmlContent = sw.toString();
				logger.debug(xmlContent);

				logger.info("Check Unmarshalling");
				Path path = Files.createTempFile("DetachedREsData", ".xml");
				logger.info("Create file: " + path.toAbsolutePath());
				File file = path.toFile();
				jaxbMarshaller.marshal(detachedREs, file);

				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				DetachedREsJAXB configUnmarshalled = (DetachedREsJAXB) jaxbUnmarshaller.unmarshal(file);
				logger.debug("DetachedREs unmarshallded: " + configUnmarshalled);
				// file.delete();
				logger.info("Success!");
				assertTrue(true);

			} catch (Throwable e) {
				logger.error(e.getLocalizedMessage(), e);
				assertTrue("Error in DetachedREs Marshal!", false);
			}
		} else {
			logger.debug("Test Disabled");
			assertTrue(true);
		}
	}

	
	private void createEOSCSecretariatDetachedGateway(GatewayJAXB eoscSecretariatDetachedGateway) {
		// D4OS
		VOJAXB d4osOfEOSCSecretariatDetachedGateway = new VOJAXB();
		d4osOfEOSCSecretariatDetachedGateway
				.setScope("/d4science.research-infrastructures.eu/" + VOProduction.D4OS.getId());
		d4osOfEOSCSecretariatDetachedGateway.setName(VOProduction.D4OS.getId());

		LinkedHashMap<String, VREJAXB> d4osVREsOfEOSCSecretariatDetachedGateway = new LinkedHashMap<>();

		String[] d4osVREsOfEOSCSecretariatDetachedGatewayArray = { "INFRAEOSC5ProjectsCollaboratory" };

		for (String vre : d4osVREsOfEOSCSecretariatDetachedGatewayArray) {
			VREJAXB d4osVREJAXB = new VREJAXB();
			d4osVREJAXB.setScope("/d4science.research-infrastructures.eu/" + VOProduction.D4OS.getId() + "/" + vre);
			d4osVREJAXB.setName(new String(vre).replace("_", " "));
			d4osVREJAXB.setStartDate(new GregorianCalendar(2018, GregorianCalendar.JANUARY, 1).getTime());
			d4osVREJAXB.setEndDate(new GregorianCalendar(2020, GregorianCalendar.MARCH, 3).getTime());
			d4osVREJAXB.setCatalogUrl("http://data.d4science.org/ctlg/" + vre);
			d4osVREJAXB.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));
			d4osVREsOfEOSCSecretariatDetachedGateway.put(
					"/d4science.research-infrastructures.eu/" + VOProduction.D4OS.getId() + "/" + vre, d4osVREJAXB);
		}
		d4osOfEOSCSecretariatDetachedGateway.setVres(d4osVREsOfEOSCSecretariatDetachedGateway);

		LinkedHashMap<String, VOJAXB> vosOfEOSCSecretariatDetachedGateway = new LinkedHashMap<>();
		vosOfEOSCSecretariatDetachedGateway.put("/d4science.research-infrastructures.eu/" + VOProduction.D4OS.getId(),
				d4osOfEOSCSecretariatDetachedGateway);

		eoscSecretariatDetachedGateway.setVos(vosOfEOSCSecretariatDetachedGateway);
	}

	private void createAginfraPlusDetachedGateway(GatewayJAXB aginfraPlusDetachedGateway) {
		// FARM
		VOJAXB farmOfAginfraPlusDetachedGateway = new VOJAXB();
		farmOfAginfraPlusDetachedGateway
				.setScope("/d4science.research-infrastructures.eu/" + VOProduction.FARM.getId());
		farmOfAginfraPlusDetachedGateway.setName(VOProduction.FARM.getId());

		LinkedHashMap<String, VREJAXB> farmVREsOfAginfraPlusDetachedGateway = new LinkedHashMap<>();

		String[] farmVREsOfAginfraPlusDetachedGatewayArray = { "EOSCFood" };

		for (String vre : farmVREsOfAginfraPlusDetachedGatewayArray) {
			VREJAXB farmVREJAXB = new VREJAXB();
			farmVREJAXB.setScope("/d4science.research-infrastructures.eu/" + VOProduction.FARM.getId() + "/" + vre);
			farmVREJAXB.setName(new String(vre).replace("_", " "));
			farmVREJAXB.setStartDate(new GregorianCalendar(2018, GregorianCalendar.JANUARY, 1).getTime());
			farmVREJAXB.setEndDate(new GregorianCalendar(2020, GregorianCalendar.MARCH, 3).getTime());
			farmVREJAXB.setCatalogUrl("http://data.d4science.org/ctlg/" + vre);
			farmVREJAXB.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));
			farmVREsOfAginfraPlusDetachedGateway.put(
					"/d4science.research-infrastructures.eu/" + VOProduction.FARM.getId() + "/" + vre, farmVREJAXB);
		}
		farmOfAginfraPlusDetachedGateway.setVres(farmVREsOfAginfraPlusDetachedGateway);

		LinkedHashMap<String, VOJAXB> vosOfAginfraPlusDetachedGateway = new LinkedHashMap<>();
		vosOfAginfraPlusDetachedGateway.put("/d4science.research-infrastructures.eu/" + VOProduction.FARM.getId(),
				farmOfAginfraPlusDetachedGateway);

		aginfraPlusDetachedGateway.setVos(vosOfAginfraPlusDetachedGateway);

	}

	private void createParthenosDetachedGateway(GatewayJAXB parthenosDetachedGateway) {

		// ParthenosVO
		VOJAXB parthenosVOOfParthenosDetachedGateway = new VOJAXB();
		parthenosVOOfParthenosDetachedGateway
				.setScope("/d4science.research-infrastructures.eu/" + VOProduction.PARTHENOSVO.getId());
		parthenosVOOfParthenosDetachedGateway.setName(VOProduction.PARTHENOSVO.getId());

		LinkedHashMap<String, VREJAXB> parthenosVOVREsOfParthenosDetachedGateway = new LinkedHashMap<>();

		String[] parthenosVOVREsOfParthenosDetachedGatewayArray = { "RubRIcA" };

		for (String vre : parthenosVOVREsOfParthenosDetachedGatewayArray) {
			VREJAXB parthenosVOVREJAXB = new VREJAXB();
			parthenosVOVREJAXB
					.setScope("/d4science.research-infrastructures.eu/" + VOProduction.PARTHENOSVO.getId() + "/" + vre);
			parthenosVOVREJAXB.setName(new String(vre).replace("_", " "));
			parthenosVOVREJAXB.setStartDate(new GregorianCalendar(2018, GregorianCalendar.JANUARY, 1).getTime());
			parthenosVOVREJAXB.setEndDate(new GregorianCalendar(2020, GregorianCalendar.MARCH, 3).getTime());
			parthenosVOVREJAXB.setCatalogUrl("http://data.d4science.org/ctlg/" + vre);
			parthenosVOVREJAXB.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));
			parthenosVOVREsOfParthenosDetachedGateway.put(
					"/d4science.research-infrastructures.eu/" + VOProduction.PARTHENOSVO.getId() + "/" + vre,
					parthenosVOVREJAXB);
		}
		parthenosVOOfParthenosDetachedGateway.setVres(parthenosVOVREsOfParthenosDetachedGateway);

		// D4Research
		VOJAXB d4ResearchOfParthenosDetachedGateway = new VOJAXB();
		d4ResearchOfParthenosDetachedGateway
				.setScope("/d4science.research-infrastructures.eu/" + VOProduction.D4RESEARCH.getId());

		d4ResearchOfParthenosDetachedGateway.setName(VOProduction.D4RESEARCH.getId());

		LinkedHashMap<String, VREJAXB> d4ResearchVREsOfParthenosDetachedGateway = new LinkedHashMap<>();

		String[] d4ResearchVREsOfParthenosDetachedGatewayArray = { "NERLiX" };

		for (String vre : d4ResearchVREsOfParthenosDetachedGatewayArray) {
			VREJAXB d4ResearchVREJAXB = new VREJAXB();
			d4ResearchVREJAXB
					.setScope("/d4science.research-infrastructures.eu/" + VOProduction.D4RESEARCH.getId() + "/" + vre);
			d4ResearchVREJAXB.setName(new String(vre).replace("_", " "));
			d4ResearchVREJAXB.setStartDate(new GregorianCalendar(2018, GregorianCalendar.JANUARY, 1).getTime());
			d4ResearchVREJAXB.setEndDate(new GregorianCalendar(2020, GregorianCalendar.MARCH, 3).getTime());
			d4ResearchVREJAXB.setCatalogUrl("http://data.d4science.org/ctlg/" + vre);
			d4ResearchVREJAXB.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));
			d4ResearchVREsOfParthenosDetachedGateway.put(
					"/d4science.research-infrastructures.eu/" + VOProduction.D4RESEARCH.getId() + "/" + vre,
					d4ResearchVREJAXB);
		}
		d4ResearchOfParthenosDetachedGateway.setVres(d4ResearchVREsOfParthenosDetachedGateway);

		LinkedHashMap<String, VOJAXB> vosOfParthenosDetachedGateway = new LinkedHashMap<>();
		vosOfParthenosDetachedGateway.put("/d4science.research-infrastructures.eu/" + VOProduction.D4RESEARCH.getId(),
				d4ResearchOfParthenosDetachedGateway);
		vosOfParthenosDetachedGateway.put("/d4science.research-infrastructures.eu/" + VOProduction.PARTHENOSVO.getId(),
				parthenosVOOfParthenosDetachedGateway);

		parthenosDetachedGateway.setVos(vosOfParthenosDetachedGateway);

	}

	private void createBlueBridgeGateway(GatewayJAXB blueBridgeGateway) {
		// gCubeApps
		VOJAXB gCubeAppsOfBlueBridgeGateway = new VOJAXB();
		gCubeAppsOfBlueBridgeGateway
				.setScope("/d4science.research-infrastructures.eu/" + VOProduction.GCUBEAPPS.getId());
		gCubeAppsOfBlueBridgeGateway.setName(VOProduction.GCUBEAPPS.getId());

		LinkedHashMap<String, VREJAXB> gCubeAppsVREsOfBlueBridgeGateway = new LinkedHashMap<>();

		String[] gCubeAppsVREsOfBluebridgeGatewayArray = { "AquacultureTrainingLab", "BlueBRIDGE-PSC",
				"BlueBridgeProject", "CES_TCRE", "ICES_DALSA", "ICES_DASC", "ICES_FIACO",
				"ICES_StockAssessmentAdvanced", "ICES_TCSSM", "iSearch", "SIASPA" };

		for (String vre : gCubeAppsVREsOfBluebridgeGatewayArray) {
			VREJAXB gCubeAppsVREJAXB = new VREJAXB();
			gCubeAppsVREJAXB
					.setScope("/d4science.research-infrastructures.eu/" + VOProduction.GCUBEAPPS.getId() + "/" + vre);
			gCubeAppsVREJAXB.setName(new String(vre).replace("_", " "));
			gCubeAppsVREJAXB.setStartDate(new GregorianCalendar(2018, GregorianCalendar.JANUARY, 1).getTime());
			gCubeAppsVREJAXB.setEndDate(new GregorianCalendar(2020, GregorianCalendar.MARCH, 3).getTime());

			// UPDATED BY FRANCESCO
			if (vre.compareToIgnoreCase("BlueBridgeProject") == 0 || vre.compareToIgnoreCase("SIASPA") == 0) {
				gCubeAppsVREJAXB.setCatalogUrl("https://ckan-imarine.d4science.org");
				gCubeAppsVREJAXB
						.setCatalogPortletURL("https://i-marine.d4science.org/group/imarine-gateway/data-catalogue");
			} else {
				// not usable
				gCubeAppsVREJAXB.setCatalogUrl("http://data.d4science.org/ctlg/" + vre);
			}

			gCubeAppsVREJAXB.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));
			gCubeAppsVREsOfBlueBridgeGateway.put(
					"/d4science.research-infrastructures.eu/" + VOProduction.GCUBEAPPS.getId() + "/" + vre,
					gCubeAppsVREJAXB);
		}
		gCubeAppsOfBlueBridgeGateway.setVres(gCubeAppsVREsOfBlueBridgeGateway);

		// FARM
		VOJAXB farmOfBlueBridgeGateway = new VOJAXB();
		farmOfBlueBridgeGateway.setScope("/d4science.research-infrastructures.eu/" + VOProduction.FARM.getId());
		farmOfBlueBridgeGateway.setName(VOProduction.FARM.getId());

		LinkedHashMap<String, VREJAXB> farmVREsOfBlueBridgeGateway = new LinkedHashMap<>();

		String[] farmVREsOfBlueBridgeGatewayArray = { "AlieiaVRE", "Aquabiotech", "ARDAG_Aquaculture",
				"EllinikaPsariaVRE", "ForkysVRE", "GALAXIDI", "iLKNAK_Aquaculture", "KIMAGRO_Fishfarming",
				"MARKELLOS_Aquaculture", "NHREUS_Aquaculture", "STRATOS_AQUACULTURES", "SustainableBlueEconomy",
				"TBTI_VRE" };

		for (String vre : farmVREsOfBlueBridgeGatewayArray) {
			VREJAXB farmVREJAXB = new VREJAXB();
			farmVREJAXB.setScope("/d4science.research-infrastructures.eu/" + VOProduction.FARM.getId() + "/" + vre);
			farmVREJAXB.setName(new String(vre).replace("_", " "));
			farmVREJAXB.setStartDate(new GregorianCalendar(2018, GregorianCalendar.JANUARY, 1).getTime());
			farmVREJAXB.setEndDate(new GregorianCalendar(2020, GregorianCalendar.MARCH, 3).getTime());
			farmVREJAXB.setCatalogUrl("http://data.d4science.org/ctlg/" + vre);
			farmVREJAXB.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));
			farmVREsOfBlueBridgeGateway.put(
					"/d4science.research-infrastructures.eu/" + VOProduction.FARM.getId() + "/" + vre, farmVREJAXB);
		}
		farmOfBlueBridgeGateway.setVres(farmVREsOfBlueBridgeGateway);

		// D4Research
		VOJAXB d4ResearchOfBlueBridgeGateway = new VOJAXB();
		d4ResearchOfBlueBridgeGateway
				.setScope("/d4science.research-infrastructures.eu/" + VOProduction.D4RESEARCH.getId());

		d4ResearchOfBlueBridgeGateway.setName(VOProduction.D4RESEARCH.getId());

		LinkedHashMap<String, VREJAXB> d4ResearchVREsOfBlueBridgeGateway = new LinkedHashMap<>();

		String[] d4ResearchVREsOfBlueBridgeGatewayArray = { "Blue-Datathon", "BlueBRIDGEReview", "BOBLME_HilsaAWG",
				"DRuMFISH", "ICES_AbundanceEstimationFromAcoustic", "ICES_BNetworkAnalysis", "ICES_FIACO2017",
				"ICES_IntroStockAssessment", "ICES_IntroToREnv", "ICES_LogbookData", "ICES_MSE", "ICES_MSY",
				"InfraTraining", "Sinay", "StatnMap" };

		for (String vre : d4ResearchVREsOfBlueBridgeGatewayArray) {
			VREJAXB d4ResearchVREJAXB = new VREJAXB();
			d4ResearchVREJAXB
					.setScope("/d4science.research-infrastructures.eu/" + VOProduction.D4RESEARCH.getId() + "/" + vre);
			d4ResearchVREJAXB.setName(new String(vre).replace("_", " "));
			d4ResearchVREJAXB.setStartDate(new GregorianCalendar(2018, GregorianCalendar.JANUARY, 1).getTime());
			d4ResearchVREJAXB.setEndDate(new GregorianCalendar(2020, GregorianCalendar.MARCH, 3).getTime());
			d4ResearchVREJAXB.setCatalogUrl("http://data.d4science.org/ctlg/" + vre);
			d4ResearchVREJAXB.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));
			d4ResearchVREsOfBlueBridgeGateway.put(
					"/d4science.research-infrastructures.eu/" + VOProduction.D4RESEARCH.getId() + "/" + vre,
					d4ResearchVREJAXB);
		}
		d4ResearchOfBlueBridgeGateway.setVres(d4ResearchVREsOfBlueBridgeGateway);

		LinkedHashMap<String, VOJAXB> vosOfBlueBridgeGateway = new LinkedHashMap<>();
		vosOfBlueBridgeGateway.put("/d4science.research-infrastructures.eu/" + VOProduction.D4RESEARCH.getId(),
				d4ResearchOfBlueBridgeGateway);
		vosOfBlueBridgeGateway.put("/d4science.research-infrastructures.eu/" + VOProduction.FARM.getId(),
				farmOfBlueBridgeGateway);
		vosOfBlueBridgeGateway.put("/d4science.research-infrastructures.eu/" + VOProduction.GCUBEAPPS.getId(),
				gCubeAppsOfBlueBridgeGateway);

		blueBridgeGateway.setVos(vosOfBlueBridgeGateway);
	}

	private void createD4ScienceOrgDetachedGateway(GatewayJAXB d4ScienceOrgDetachedGateway) {
		// gCubeApps
		VOJAXB gCubeAppsOfD4ScienceOrgDetachedGateway = new VOJAXB();
		gCubeAppsOfD4ScienceOrgDetachedGateway
				.setScope("/d4science.research-infrastructures.eu/" + VOProduction.GCUBEAPPS.getId());
		gCubeAppsOfD4ScienceOrgDetachedGateway.setName(VOProduction.GCUBEAPPS.getId());

		LinkedHashMap<String, VREJAXB> gCubeAppsVREsOfD4ScienceOrgDetachedGateway = new LinkedHashMap<>();

		String[] gCubeAppsVREsOfD4ScienceOrgDetachedGatewayArray = { "CNR_OpenScienceTF", "EcologicalModelling",
				"EGIEngage", "EGIP", "ICOS_ETC", "IGDI", "rScience", "TCom" };

		for (String vre : gCubeAppsVREsOfD4ScienceOrgDetachedGatewayArray) {
			VREJAXB gCubeAppsVREJAXB = new VREJAXB();
			gCubeAppsVREJAXB
					.setScope("/d4science.research-infrastructures.eu/" + VOProduction.GCUBEAPPS.getId() + "/" + vre);
			gCubeAppsVREJAXB.setName(new String(vre).replace("_", " "));
			gCubeAppsVREJAXB.setStartDate(new GregorianCalendar(2018, GregorianCalendar.JANUARY, 1).getTime());
			gCubeAppsVREJAXB.setEndDate(new GregorianCalendar(2020, GregorianCalendar.MARCH, 3).getTime());
			gCubeAppsVREJAXB.setCatalogUrl("http://data.d4science.org/ctlg/" + vre);
			gCubeAppsVREJAXB.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));
			gCubeAppsVREsOfD4ScienceOrgDetachedGateway.put(
					"/d4science.research-infrastructures.eu/" + VOProduction.GCUBEAPPS.getId() + "/" + vre,
					gCubeAppsVREJAXB);
		}
		gCubeAppsOfD4ScienceOrgDetachedGateway.setVres(gCubeAppsVREsOfD4ScienceOrgDetachedGateway);

		// FARM
		VOJAXB farmOfD4ScienceOrgDetachedGateway = new VOJAXB();
		farmOfD4ScienceOrgDetachedGateway
				.setScope("/d4science.research-infrastructures.eu/" + VOProduction.FARM.getId());
		farmOfD4ScienceOrgDetachedGateway.setName(VOProduction.FARM.getId());

		LinkedHashMap<String, VREJAXB> farmVREsOfD4ScienceOrgDetachedGateway = new LinkedHashMap<>();

		String[] farmVREsOfD4ScienceOrgDetachedGatewayArray = { "EUChinaFoodSTAR" };

		for (String vre : farmVREsOfD4ScienceOrgDetachedGatewayArray) {
			VREJAXB farmVREJAXB = new VREJAXB();
			farmVREJAXB.setScope("/d4science.research-infrastructures.eu/" + VOProduction.FARM.getId() + "/" + vre);
			farmVREJAXB.setName(new String(vre).replace("_", " "));
			farmVREJAXB.setStartDate(new GregorianCalendar(2018, GregorianCalendar.JANUARY, 1).getTime());
			farmVREJAXB.setEndDate(new GregorianCalendar(2020, GregorianCalendar.MARCH, 3).getTime());
			farmVREJAXB.setCatalogUrl("http://data.d4science.org/ctlg/" + vre);
			farmVREJAXB.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));
			farmVREsOfD4ScienceOrgDetachedGateway.put(
					"/d4science.research-infrastructures.eu/" + VOProduction.FARM.getId() + "/" + vre, farmVREJAXB);
		}
		farmOfD4ScienceOrgDetachedGateway.setVres(farmVREsOfD4ScienceOrgDetachedGateway);

		// D4Research
		VOJAXB d4ResearchOfD4ScienceOrgDetachedGateway = new VOJAXB();
		d4ResearchOfD4ScienceOrgDetachedGateway
				.setScope("/d4science.research-infrastructures.eu/" + VOProduction.D4RESEARCH.getId());

		d4ResearchOfD4ScienceOrgDetachedGateway.setName(VOProduction.D4RESEARCH.getId());

		LinkedHashMap<String, VREJAXB> d4ResearchVREsOfD4ScienceOrgDetachedGateway = new LinkedHashMap<>();

		String[] d4ResearchVREsOfD4ScienceOrgDetachedGatewayArray = { "EISCAT", "ENVRI", "EOSC_Services", "FAIR_DM",
				"FisheriesAndEcosystemAtMii", "ICOSEddyCovarianceProcessing", "ISTIOpenAccess", "QCAPI" };

		for (String vre : d4ResearchVREsOfD4ScienceOrgDetachedGatewayArray) {
			VREJAXB d4ResearchVREJAXB = new VREJAXB();
			d4ResearchVREJAXB
					.setScope("/d4science.research-infrastructures.eu/" + VOProduction.D4RESEARCH.getId() + "/" + vre);
			d4ResearchVREJAXB.setName(new String(vre).replace("_", " "));
			d4ResearchVREJAXB.setStartDate(new GregorianCalendar(2018, GregorianCalendar.JANUARY, 1).getTime());
			d4ResearchVREJAXB.setEndDate(new GregorianCalendar(2020, GregorianCalendar.MARCH, 3).getTime());
			d4ResearchVREJAXB.setCatalogUrl("http://data.d4science.org/ctlg/" + vre);
			d4ResearchVREJAXB.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));
			d4ResearchVREsOfD4ScienceOrgDetachedGateway.put(
					"/d4science.research-infrastructures.eu/" + VOProduction.D4RESEARCH.getId() + "/" + vre,
					d4ResearchVREJAXB);
		}
		d4ResearchOfD4ScienceOrgDetachedGateway.setVres(d4ResearchVREsOfD4ScienceOrgDetachedGateway);

		// D4OS
		VOJAXB d4osOfD4ScienceOrgDetachedGateway = new VOJAXB();
		d4osOfD4ScienceOrgDetachedGateway
				.setScope("/d4science.research-infrastructures.eu/" + VOProduction.D4OS.getId());
		d4osOfD4ScienceOrgDetachedGateway.setName(VOProduction.D4OS.getId());

		LinkedHashMap<String, VREJAXB> d4osVREsOfD4ScienceOrgDetachedGateway = new LinkedHashMap<>();

		String[] d4osVREsOfD4ScienceOrgDetachedGatewayArray = { "CNROutreach", "RicAt" };

		for (String vre : d4osVREsOfD4ScienceOrgDetachedGatewayArray) {
			VREJAXB d4osVREJAXB = new VREJAXB();
			d4osVREJAXB.setScope("/d4science.research-infrastructures.eu/" + VOProduction.D4OS.getId() + "/" + vre);
			d4osVREJAXB.setName(new String(vre).replace("_", " "));
			d4osVREJAXB.setStartDate(new GregorianCalendar(2018, GregorianCalendar.JANUARY, 1).getTime());
			d4osVREJAXB.setEndDate(new GregorianCalendar(2020, GregorianCalendar.MARCH, 3).getTime());
			d4osVREJAXB.setCatalogUrl("http://data.d4science.org/ctlg/" + vre);
			d4osVREJAXB.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));
			d4osVREsOfD4ScienceOrgDetachedGateway.put(
					"/d4science.research-infrastructures.eu/" + VOProduction.D4OS.getId() + "/" + vre, d4osVREJAXB);
		}
		d4osOfD4ScienceOrgDetachedGateway.setVres(d4osVREsOfD4ScienceOrgDetachedGateway);

		//
		LinkedHashMap<String, VOJAXB> vosOfD4ScienceOrgDetachedGateway = new LinkedHashMap<>();
		vosOfD4ScienceOrgDetachedGateway.put("/d4science.research-infrastructures.eu/" + VOProduction.D4OS.getId(),
				d4osOfD4ScienceOrgDetachedGateway);
		vosOfD4ScienceOrgDetachedGateway.put(
				"/d4science.research-infrastructures.eu/" + VOProduction.D4RESEARCH.getId(),
				d4ResearchOfD4ScienceOrgDetachedGateway);
		vosOfD4ScienceOrgDetachedGateway.put("/d4science.research-infrastructures.eu/" + VOProduction.FARM.getId(),
				farmOfD4ScienceOrgDetachedGateway);
		vosOfD4ScienceOrgDetachedGateway.put("/d4science.research-infrastructures.eu/" + VOProduction.GCUBEAPPS.getId(),
				gCubeAppsOfD4ScienceOrgDetachedGateway);

		d4ScienceOrgDetachedGateway.setVos(vosOfD4ScienceOrgDetachedGateway);
	}

	private void createDescrambleGateway(GatewayJAXB descrambleGateway) {
		// gCubeApps
		VOJAXB gCubeAppsOfDescrambleGateway = new VOJAXB();
		gCubeAppsOfDescrambleGateway
				.setScope("/d4science.research-infrastructures.eu/" + VOProduction.GCUBEAPPS.getId());
		gCubeAppsOfDescrambleGateway.setName(VOProduction.GCUBEAPPS.getId());

		LinkedHashMap<String, VREJAXB> gCubeAppsVREsOfDescrambleGateway = new LinkedHashMap<>();

		String[] gCubeAppsVREsOfDescrambleGatewayArray = { "DESCRAMBLE" };

		for (String vre : gCubeAppsVREsOfDescrambleGatewayArray) {
			VREJAXB gCubeAppsVREJAXB = new VREJAXB();
			gCubeAppsVREJAXB
					.setScope("/d4science.research-infrastructures.eu/" + VOProduction.GCUBEAPPS.getId() + "/" + vre);
			gCubeAppsVREJAXB.setName(new String(vre).replace("_", " "));
			gCubeAppsVREJAXB.setStartDate(new GregorianCalendar(2014, GregorianCalendar.JANUARY, 1).getTime());
			gCubeAppsVREJAXB.setEndDate(new GregorianCalendar(2020, GregorianCalendar.SEPTEMBER, 25).getTime());
			gCubeAppsVREJAXB.setCatalogUrl("http://data.d4science.org/ctlg/" + vre);
			gCubeAppsVREJAXB.setManagers(new ArrayList<String>(Arrays.asList("Leonardo Candela", "Pasquale Pagano")));
			gCubeAppsVREsOfDescrambleGateway.put(
					"/d4science.research-infrastructures.eu/" + VOProduction.GCUBEAPPS.getId() + "/" + vre,
					gCubeAppsVREJAXB);
		}
		gCubeAppsOfDescrambleGateway.setVres(gCubeAppsVREsOfDescrambleGateway);

		LinkedHashMap<String, VOJAXB> vosOfDescrambleGateway = new LinkedHashMap<>();
		vosOfDescrambleGateway.put("/d4science.research-infrastructures.eu/" + VOProduction.GCUBEAPPS.getId(),
				gCubeAppsOfDescrambleGateway);

		descrambleGateway.setVos(vosOfDescrambleGateway);
	}

}
