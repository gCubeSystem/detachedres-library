package org.gcube.infrastructure.detachedres.detachedreslibrary;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public enum VOProduction {

	GCUBEAPPS("gCubeApps"), FARM("FARM"), D4RESEARCH("D4Research"), PARTHENOSVO("ParthenosVO"), D4OS("D4OS");

	String id;

	VOProduction(String id) {
		this.id = id;
	}

	public String getId() {
		return this.id;
	}
	

}
