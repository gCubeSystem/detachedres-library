package org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.obj;

import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * 
 * @author Giancarlo Panichi
 * 
 * @author updated by Francesco Mangiacrapa at ISTI-CNR
 */
@XmlRootElement(name = "vre")
@XmlAccessorType(XmlAccessType.FIELD)
public class VREJAXB {
	@XmlElement
	private String scope;

	@XmlElement
	private String name;

	@XmlElement(name = "description", required = false)
	private String description;

	@XmlElementWrapper(name = "managers", required = false)
	@XmlElement(name = "manager", required = false)
	private ArrayList<String> managers;

	@XmlElement(name = "startdate", required = false)
	@XmlJavaTypeAdapter(DateTimeAdapter.class)
	private Date startDate;

	@XmlElement(name = "enddate", required = false)
	@XmlJavaTypeAdapter(DateTimeAdapter.class)
	private Date endDate;

	@XmlElement(name = "catalogurl", required = false)
	private String catalogUrl;

	@XmlElement(name = "catalogportleturl", required = false)
	private String catalogPortletURL;

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<String> getManagers() {
		return managers;
	}

	public void setManagers(ArrayList<String> managers) {
		this.managers = managers;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCatalogUrl() {
		return catalogUrl;
	}

	public void setCatalogUrl(String catalogUrl) {
		this.catalogUrl = catalogUrl;
	}

	public String getCatalogPortletURL() {
		return catalogPortletURL;
	}

	public void setCatalogPortletURL(String catalogPortletURL) {
		this.catalogPortletURL = catalogPortletURL;
	}

	@Override
	public String toString() {
		return "VREJAXB [scope=" + scope + ", name=" + name + ", description=" + description + ", managers=" + managers
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", catalogUrl=" + catalogUrl
				+ ", catalogPortletURL=" + catalogPortletURL + "]";
	}

}
