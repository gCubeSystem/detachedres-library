package org.gcube.infrastructure.detachedres.detachedreslibrary.shared;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class Constants {

	public static final boolean DEBUG_MODE = false;
	public static final boolean TEST_ENABLE = false;

	public static final String DEFAULT_USER = "giancarlo.panichi";
	// public static final String DEFAULT_SCOPE = "/gcube/devNext/NextNext";
	public static final String DEFAULT_SCOPE = "/gcube";
	public static final String DEFAULT_TOKEN = "";
	public static final String DEFAULT_ROLE = "OrganizationMember";

	// IS Resource
	public static final String DETACHED_RES_CATEGORY = "DetachedREs";
	public static final String DETACHED_RES_NAME = "DetachedREsData";

}
