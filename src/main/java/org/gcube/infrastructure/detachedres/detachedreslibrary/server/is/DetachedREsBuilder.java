package org.gcube.infrastructure.detachedres.detachedreslibrary.server.is;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;

import org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.obj.DetachedREsJAXB;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.obj.GatewayJAXB;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.obj.VOJAXB;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.obj.VREJAXB;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.Constants;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.DetachedREs;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.Gateway;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VO;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VRE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class DetachedREsBuilder {

	private static Logger logger = LoggerFactory.getLogger(DetachedREsBuilder.class);
	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	public static DetachedREs build(String scope) throws Exception {
		logger.info("Build DetachedREs");
		DetachedREs detachedREs = null;

		if (Constants.DEBUG_MODE) {
			logger.info("DetachedREsBuilder: use debug configuration.");
			detachedREs = useDefaultConfiguration();
		} else {
			DetachedREsJAXB detachedREsJAXB = null;
			try {
				detachedREsJAXB = InformationSystemUtils.retrieveDetachedREs(scope);
			} catch (Exception e) {
				logger.debug(e.getLocalizedMessage(), e);
			}

			if (detachedREsJAXB != null) {
				logger.info("DetachedREsBuilder: use configuration in scope: " + scope);

				detachedREs = new DetachedREs();
				logger.info("DetachedREsBuilder: configuration enabled: " + detachedREsJAXB.isEnabled());
				detachedREs.setEnabled(detachedREsJAXB.isEnabled());

				LinkedHashMap<String,GatewayJAXB> gatewaysJAXB = detachedREsJAXB.getGateways();
				logger.info("DetachedREsBuilder: Gateways configuration: " + gatewaysJAXB);
				if (gatewaysJAXB != null && !gatewaysJAXB.isEmpty()) {
					LinkedHashMap<String, Gateway> gateways = new LinkedHashMap<>();
					for (String key: gatewaysJAXB.keySet()) {
						GatewayJAXB gatewayJAXB =gatewaysJAXB.get(key);
						Gateway gateway = new Gateway(gatewayJAXB.getScope(), gatewayJAXB.getName(),
								gatewayJAXB.getDescription());

						if (gatewayJAXB.getStartDate() != null) {

							try {
								String startDate = dateFormat.format(gatewayJAXB.getStartDate());
								gateway.setStartDate(startDate);
							} catch (Exception e) {
								logger.error("Invalid start date format for gateway: " + gatewayJAXB.getName());
							}
						}

						if (gatewayJAXB.getEndDate() != null) {
							try {
								String endDate = dateFormat.format(gatewayJAXB.getEndDate());
								gateway.setEndDate(endDate);
							} catch (Exception e) {
								logger.error("Invalid end date format for gateway: " + gatewayJAXB.getName());

							}
						}
						gateway.setCatalogUrl(gatewayJAXB.getCatalogUrl());

						if (gatewayJAXB.getVos() != null && !gatewayJAXB.getVos().isEmpty()) {
							LinkedHashMap<String,VO> vos = retrieveVOs(gatewayJAXB);
							gateway.setVos(vos);
						}
						gateways.put(key,gateway);
					}
					detachedREs.setGateways(gateways);

				} else {
					logger.info("DetachedREsBuilder: Gateways are not present in" + " resource for scope: " + scope);
				}

			} else {
				logger.info("DetachedREsBuilder: DetachedREsData resource is not present in scope: " + scope);
			}
		}

		logger.debug("DetachedREsBuilder: DetachedREs retrieved: " + detachedREs);
		return detachedREs;
	}

	private static DetachedREs useDefaultConfiguration() {
		Gateway blueBridgeGateway = new Gateway("-1", "BlueBridge Gateway");
		VO gCubeApps = new VO("/d4science.research-infrastructures.eu/gCubeApps", "gCubeApps");
		VRE blueBridgeProject = new VRE("/d4science.research-infrastructures.eu/gCubeApps/BlueBridgeProject",
				"BlueBridgeProject");

		LinkedHashMap<String,VRE> vres = new LinkedHashMap<>();
		vres.put("/d4science.research-infrastructures.eu/gCubeApps",blueBridgeProject);
		gCubeApps.setVres(vres);

		LinkedHashMap<String,VO> vos = new LinkedHashMap<>();
		vos.put("/d4science.research-infrastructures.eu/gCubeApps",gCubeApps);
		blueBridgeGateway.setVos(vos);

		LinkedHashMap<String,Gateway> gateways = new LinkedHashMap<>();
		gateways.put("-1",blueBridgeGateway);

		DetachedREs detachedREs = new DetachedREs(true, gateways);
		return detachedREs;
	}

	private static LinkedHashMap<String,VO> retrieveVOs(GatewayJAXB gatewayJAXB) throws Exception {
		try {
			LinkedHashMap<String,VO> vos = new LinkedHashMap<>();
			for (String key : gatewayJAXB.getVos().keySet()) {
				VOJAXB voJAXB=gatewayJAXB.getVos().get(key);
				VO vo = new VO(voJAXB.getScope(), voJAXB.getName(), voJAXB.getDescription());

				if (voJAXB.getStartDate() != null) {
					try {
						String startDate = dateFormat.format(voJAXB.getStartDate());
						vo.setStartDate(startDate);
					} catch (Exception e) {
						logger.error("Invalid start date format for VO: " + voJAXB.getName());
					}
				}

				if (voJAXB.getEndDate() != null) {
					try {
						String endDate = dateFormat.format(voJAXB.getEndDate());
						vo.setEndDate(endDate);
					} catch (Exception e) {
						logger.error("Invalid end date format for VO: " + voJAXB.getName());

					}
				}
				vo.setCatalogUrl(voJAXB.getCatalogUrl());

				if (voJAXB.getVres() != null && !voJAXB.getVres().isEmpty()) {
					LinkedHashMap<String,VRE> vres = retrieveVREs(voJAXB);
					vo.setVres(vres);
				}
				vos.put(key,vo);
			}
			return vos;

		} catch (Exception e) {
			throw e;
		} catch (Throwable e) {
			logger.error("Invalid DetachedREs configuration. Error retrieving VO info: " + e.getLocalizedMessage(), e);
			throw new Exception(
					"Invalid DetachedREs configuration. Error retrieving VO info: " + e.getLocalizedMessage(), e);
		}

	}

	private static LinkedHashMap<String,VRE> retrieveVREs(VOJAXB voJAXB) throws Exception {
		try {
			LinkedHashMap<String,VRE> vres = new LinkedHashMap<>();
			for (String key : voJAXB.getVres().keySet()) {
				VREJAXB vreJAXB=voJAXB.getVres().get(key);
				VRE vre = new VRE(vreJAXB.getScope(), vreJAXB.getName(), vreJAXB.getDescription());

				if (vreJAXB.getStartDate() != null) {
					try {
						String startDate = dateFormat.format(vreJAXB.getStartDate());
						vre.setStartDate(startDate);
					} catch (Exception e) {
						logger.error("Invalid start date format for VO: " + vreJAXB.getName());
					}
				}

				if (vreJAXB.getEndDate() != null) {
					try {
						String endDate = dateFormat.format(vreJAXB.getEndDate());
						vre.setEndDate(endDate);
					} catch (Exception e) {
						logger.error("Invalid end date format for VO: " + vreJAXB.getName());

					}
				}
				vre.setCatalogUrl(vreJAXB.getCatalogUrl());
				vre.setCatalogPortletURL(vreJAXB.getCatalogPortletURL());
				vre.setManagers(vreJAXB.getManagers());
				vres.put(key,vre);
			}
			return vres;

		} catch (Exception e) {
			throw e;
		} catch (Throwable e) {
			logger.error("Invalid DetachedREs configuration. Error retrieving VRE info: " + e.getLocalizedMessage(), e);
			throw new Exception(
					"Invalid DetachedREs configuration. Error retrieving VRE info: " + e.getLocalizedMessage(), e);
		}

	}

}
