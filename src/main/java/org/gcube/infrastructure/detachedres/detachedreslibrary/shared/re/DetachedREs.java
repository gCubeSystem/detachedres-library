package org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class DetachedREs implements Serializable {

	private static final long serialVersionUID = 2238683459962419017L;
	private boolean enabled;
	private LinkedHashMap<String, Gateway> gateways;

	public DetachedREs() {
		super();
	}

	public DetachedREs(boolean enabled, LinkedHashMap<String, Gateway> gateways) {
		super();
		this.enabled = enabled;
		this.gateways = gateways;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public LinkedHashMap<String, Gateway> getGateways() {
		return gateways;
	}

	public void setGateways(LinkedHashMap<String, Gateway> gateways) {
		this.gateways = gateways;
	}

	@Override
	public String toString() {
		return "DetachedREs [enabled=" + enabled + ", gateways=" + gateways + "]";
	}

}