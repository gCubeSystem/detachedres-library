package org.gcube.infrastructure.detachedres.detachedreslibrary.server.is;

import java.util.Iterator;
import java.util.List;

import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.ScopeGroup;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.obj.DetachedREsJAXB;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.Constants;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.impl.JAXBParser;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class InformationSystemUtils {

	private static Logger logger = LoggerFactory.getLogger(InformationSystemUtils.class);

	public static DetachedREsJAXB retrieveDetachedREs(String scope) throws Exception {
		try {

			if (scope == null || scope.length() == 0)
				return null;

			ScopeProvider.instance.set(scope);
			logger.debug("Retrieve DetachedREs in scope: " + scope);

			SimpleQuery query = ICFactory.queryFor(GenericResource.class);
			query.addCondition("$resource/Profile/SecondaryType/text() eq '" + Constants.DETACHED_RES_CATEGORY + "'")
					.addCondition("$resource/Profile/Name/text() eq '" + Constants.DETACHED_RES_NAME + "'")
					.setResult("$resource");

			DiscoveryClient<GenericResource> client = ICFactory.clientFor(GenericResource.class);
			List<GenericResource> detachedREsDataResources = client.submit(query);
			logger.debug("Resources: " + detachedREsDataResources);

			DetachedREsJAXB detachedREsJAXB = null;

			for (GenericResource genericResource : detachedREsDataResources) {
				if (genericResource.scopes() != null) {
					ScopeGroup<String> scopes = genericResource.scopes();
					Iterator<String> iterator = scopes.iterator();
					String scopeFound = null;
					boolean found = false;
					while (iterator.hasNext() && !found) {
						scopeFound = iterator.next();
						if (scopeFound.compareTo(scope) == 0) {
							found = true;
						}
					}
					if (found) {
						try {
							JAXBParser<DetachedREsJAXB> parser = new JAXBParser<DetachedREsJAXB>(DetachedREsJAXB.class);
							logger.debug("Body: " + genericResource.profile().bodyAsString());
							detachedREsJAXB = (DetachedREsJAXB) parser.parse(genericResource.profile().bodyAsString());
							logger.debug("DetachedREs: " + detachedREsJAXB);
						} catch (Throwable e) {
							String error = "Error in discovery DetachedREs in scope " + scope + ". "
									+ "Resource parsing failed!";
							logger.error(error);
							logger.error(
									"Error {resource=" + genericResource + ", error=" + e.getLocalizedMessage() + "}");
							logger.error(e.getLocalizedMessage(), e);
							throw new Exception(error, e);
						}
						break;

					}

				}
			}

			return detachedREsJAXB;

		} catch (Exception e) {
			throw e;
		} catch (Throwable e) {
			String error = "Error in discovery DetachedREs in scope: " + scope;
			logger.error(error, e);
			throw new Exception(error, e);
		}
	}

}
