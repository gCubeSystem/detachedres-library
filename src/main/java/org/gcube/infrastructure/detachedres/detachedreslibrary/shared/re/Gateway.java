package org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class Gateway implements Serializable, Comparable<Gateway> {

	private static final long serialVersionUID = -7369449849642474360L;
	private String scope;
	private String name;
	private String description;
	private LinkedHashMap<String, VO> vos;
	private String startDate;
	private String endDate;
	private String catalogUrl;

	public Gateway() {
		super();
	}

	public Gateway(String scope, String name) {
		super();
		this.scope = scope;
		this.name = name;
	}

	public Gateway(String scope, String name, String description) {
		super();
		this.scope = scope;
		this.name = name;
		this.description = description;
	}

	public Gateway(String scope, String name, String description, LinkedHashMap<String, VO> vos) {
		super();
		this.scope = scope;
		this.name = name;
		this.description = description;
		this.vos = vos;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LinkedHashMap<String, VO> getVos() {
		return vos;
	}

	public void setVos(LinkedHashMap<String, VO> vos) {
		this.vos = vos;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCatalogUrl() {
		return catalogUrl;
	}

	public void setCatalogUrl(String catalogUrl) {
		this.catalogUrl = catalogUrl;
	}

	@Override
	public int compareTo(Gateway gateway) {
		if (name == null) {
			return -1;
		} else {
			if (gateway != null && gateway.getName() != null) {
				return name.compareTo(gateway.getName());
			} else {
				return 1;
			}
		}
	}

	@Override
	public String toString() {
		return "Gateway [scope=" + scope + ", name=" + name + ", description=" + description + ", vos=" + vos
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", catalogUrl=" + catalogUrl + "]";
	}

}