package org.gcube.infrastructure.detachedres.detachedreslibrary.server;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.DetachedREsBuilder;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.Constants;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.DetachedREs;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.Gateway;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VO;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VRE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DetachedREsClient.
 *
 * @author Giancarlo Panichi
 * 
 * @author updated by Francesco Mangiacrapa at ISTI-CNR
 */
public class DetachedREsClient {

	private Logger logger = LoggerFactory.getLogger(DetachedREsClient.class);
	private String token;
	private String userName;
	private String scope;

	/**
	 * Instantiates a new detached R es client.
	 */
	public DetachedREsClient() {

	}

	/**
	 * Instantiates a new detached R es client.
	 *
	 * @param token the token
	 */
	public DetachedREsClient(String token) {
		this.token = token;
	}

	/**
	 * Gets the detached R es.
	 *
	 * @return the detached R es
	 * @throws Exception the exception
	 */
	public DetachedREs getDetachedREs() throws Exception {
		retrieveAuthInfo();
		logger.debug("Use: [userName={}, scope={}, token={}]",userName,scope,token);
		return DetachedREsBuilder.build(scope);

	}

	/**
	 * Gets the detached R es in scope.
	 *
	 * @param scope the scope
	 * @return the detached R es in scope
	 * @throws Exception the exception
	 */
	public DetachedREs getDetachedREsInScope(String scope) throws Exception {
		retrieveAuthInfo();
		this.scope=scope;
		logger.debug("Use: [userName={}, scope={}, token={}]",userName,scope,token);
		return DetachedREsBuilder.build(scope);

	}
	
	/**
	 * Retrieve auth info.
	 *
	 * @throws Exception the exception
	 */
	private void retrieveAuthInfo() throws Exception {
		if (Constants.DEBUG_MODE) {
			logger.debug("Debug Mode");
			userName = Constants.DEFAULT_USER;
			scope = Constants.DEFAULT_SCOPE;
			token = Constants.DEFAULT_TOKEN;
			
			
		} else {
			logger.debug("Production Mode");
			try {
				if (token == null) {
					logger.debug("Retrieving token credentials");
					token = SecurityTokenProvider.instance.get();
				}
				logger.debug("Use token: " + token);
				AuthorizationEntry entry = authorizationService().get(token);
				userName = entry.getClientInfo().getId();
				scope = entry.getContext();
			} catch (Exception e) {
				logger.error("Error Retrieving token credentials: " + e.getLocalizedMessage(), e);
				throw e;
			}

		}
		
	
	}
	
	
	/**
	 * Find detached VRE for VRE name.
	 *
	 * @param vreName the vre name
	 * @return the vre detached if it is found.
	 */
	public VRE findDetachedVREforVREName(String vreName) {
		logger.debug("Find the VRE name: "+vreName+" called");
		try {
			
			DetachedREs detachedREs = getDetachedREs();
		
			for (Gateway gateway : detachedREs.getGateways().values()) {
				for (VO vo : gateway.getVos().values()) {
					for (VRE vre : vo.getVres().values()) {
						logger.trace("Does the scope: "+vre.getScope() +" ends with "+vreName +"?");
						if(vre.getScope().toLowerCase().endsWith(vreName)){
							logger.info("The scope: "+vre.getScope() +" ends with: "+vreName);
							return vre;
						}
					}
				}
			}
			
			logger.info("The VRE name "+vreName+" was not found in the detached VREs");
			return null;

		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			return null;
		}
	}

}
