package org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class VRE implements Serializable, Comparable<VRE> {

	private static final long serialVersionUID = 8250701755399181125L;
	private String scope;
	private String name;
	private String description;
	private ArrayList<String> managers;
	private String startDate;
	private String endDate;
	private String catalogUrl;
	private String catalogPortletURL;

	public VRE() {
		super();
	}

	public VRE(String scope, String name) {
		super();
		this.scope = scope;
		this.name = name;
	}

	public VRE(String scope, String name, String description) {
		super();
		this.scope = scope;
		this.name = name;
		this.description = description;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<String> getManagers() {
		return managers;
	}

	public void setManagers(ArrayList<String> managers) {
		this.managers = managers;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCatalogUrl() {
		return catalogUrl;
	}

	public void setCatalogUrl(String catalogUrl) {
		this.catalogUrl = catalogUrl;
	}
	
	
	
	public String getCatalogPortletURL() {
		return catalogPortletURL;
	}

	public void setCatalogPortletURL(String catalogPortletURL) {
		this.catalogPortletURL = catalogPortletURL;
	}

	@Override
	public int compareTo(VRE vre) {
		if (name == null) {
			return -1;
		} else {
			if (vre != null && vre.getName() != null) {
				return name.compareTo(vre.getName());
			} else {
				return 1;
			}
		}
	}

	@Override
	public String toString() {
		return "VRE [scope=" + scope + ", name=" + name + ", description=" + description + ", managers=" + managers
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", catalogUrl=" + catalogUrl
				+ ", catalogPortletURL=" + catalogPortletURL + "]";
	}

	

}