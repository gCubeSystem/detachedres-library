package org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.obj;

import java.util.Date;
import java.util.LinkedHashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
@XmlRootElement(name = "vo")
@XmlAccessorType(XmlAccessType.FIELD)
public class VOJAXB {
	@XmlElement
	private String scope;

	@XmlElement
	private String name;

	@XmlElement(name = "description", required = false)
	private String description;

	@XmlElementWrapper(name = "vres", required = false)
	private LinkedHashMap<String,VREJAXB> vres;

	@XmlElement(name = "startdate", required = false)
	@XmlJavaTypeAdapter(DateTimeAdapter.class)
	private Date startDate;

	@XmlElement(name = "enddate", required = false)
	@XmlJavaTypeAdapter(DateTimeAdapter.class)
	private Date endDate;

	@XmlElement(name = "catalogurl", required = false)
	private String catalogUrl;

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LinkedHashMap<String, VREJAXB> getVres() {
		return vres;
	}

	public void setVres(LinkedHashMap<String, VREJAXB> vres) {
		this.vres = vres;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCatalogUrl() {
		return catalogUrl;
	}

	public void setCatalogUrl(String catalogUrl) {
		this.catalogUrl = catalogUrl;
	}

	@Override
	public String toString() {
		return "VOJAXB [scope=" + scope + ", name=" + name + ", description=" + description + ", vres=" + vres
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", catalogUrl=" + catalogUrl + "]";
	}

}
