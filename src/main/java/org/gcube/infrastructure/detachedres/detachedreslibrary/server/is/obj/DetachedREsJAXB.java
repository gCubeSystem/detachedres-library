package org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.obj;

import java.util.LinkedHashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
@XmlRootElement(name = "detachedres")
@XmlAccessorType(XmlAccessType.FIELD)
public class DetachedREsJAXB {

	@XmlElement(name = "enabled")
	private boolean enabled;

	@XmlElementWrapper(name = "gateways", required = false)
	private LinkedHashMap<String,GatewayJAXB> gateways;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public LinkedHashMap<String, GatewayJAXB> getGateways() {
		return gateways;
	}

	public void setGateways(LinkedHashMap<String, GatewayJAXB> gateways) {
		this.gateways = gateways;
	}

	@Override
	public String toString() {
		return "DetachedREsJAXB [enabled=" + enabled + ", gateways=" + gateways + "]";
	}


}
