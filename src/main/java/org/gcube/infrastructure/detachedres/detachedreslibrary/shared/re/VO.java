package org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class VO implements Serializable, Comparable<VO> {

	private static final long serialVersionUID = 3227628150807395623L;
	private String scope;
	private String name;
	private String description;
	private LinkedHashMap<String, VRE> vres;
	private String startDate;
	private String endDate;
	private String catalogUrl;

	public VO() {
		super();
	}

	public VO(String scope, String name) {
		super();
		this.scope = scope;
		this.name = name;
	}

	public VO(String scope, String name, String description) {
		super();
		this.scope = scope;
		this.name = name;
		this.description = description;
	}

	public VO(String scope, String name, String description, LinkedHashMap<String, VRE> vres) {
		super();
		this.scope = scope;
		this.name = name;
		this.description = description;
		this.vres = vres;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LinkedHashMap<String, VRE> getVres() {
		return vres;
	}

	public void setVres(LinkedHashMap<String, VRE> vres) {
		this.vres = vres;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCatalogUrl() {
		return catalogUrl;
	}

	public void setCatalogUrl(String catalogUrl) {
		this.catalogUrl = catalogUrl;
	}

	@Override
	public int compareTo(VO vo) {
		if (name == null) {
			return -1;
		} else {
			if (vo != null && vo.getName() != null) {
				return name.compareTo(vo.getName());
			} else {
				return 1;
			}
		}
	}

	@Override
	public String toString() {
		return "VO [scope=" + scope + ", name=" + name + ", description=" + description + ", vres=" + vres
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", catalogUrl=" + catalogUrl + "]";
	}

}